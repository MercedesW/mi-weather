import React from "react";
import ForecastCard from "./ForecastCard";
import Details from "./Details";

class ForecastStrip extends React.Component {
  render() {
    return (
      <div>
        <div>
          <ForecastCard />
        </div>
        <div>
          <ForecastCard />
        </div>
        <div>
          <ForecastCard />
        </div>
        <div>
          <ForecastCard />
        </div>
        <div>
          <ForecastCard />
        </div>
        <div className="details-strip">
          <h4 className="forecast-title">Forecast Details for: </h4>
          <Details />
        </div>
      </div>

    );
  }
}

export default ForecastStrip;
