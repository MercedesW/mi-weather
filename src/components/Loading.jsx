import React from "react";

const Loading = () => (
  <div
    className="spinner-border text-info"
    style={{ width: "3rem", height: "3rem" }}
    role="status"
  >
    <span className="sr-only">Loading...</span>
  </div>
);

export default Loading;
