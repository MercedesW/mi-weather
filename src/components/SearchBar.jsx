import React from "react";

const SearchBar = props => (
  <form>
    <div>
      <input
        className="Bar-box"
        type="text"
        placeholder="City or City,Country"
      />
      <button>Check Weather</button>
    </div>
  </form>
);

export default SearchBar;