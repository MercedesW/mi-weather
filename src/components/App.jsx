import React from "react";
import "babel-polyfill";
import SearchBar from "./SearchBar";
import WeatherTabs from "./WeatherTabs";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-pretitle">Welcome to</h1>
          <h2 className="App-title">MayWeather!</h2>
        </header>
        <SearchBar />
        <WeatherTabs />
      </div>
    );
  }
}


export default App;
