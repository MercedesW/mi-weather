import React from "react";

const ForecastCard = props => {
  return (
    <div className="forecast-card">
      <h2 className="card-dayName">
        <strong>Lun</strong>
      </h2>
      <h3>20/5</h3>
      <h3>Icon</h3>
      <h3 className="temp-max">Max</h3>
      <h3 className="temp-min">Min</h3>
    </div>
  );
};

export default ForecastCard;
