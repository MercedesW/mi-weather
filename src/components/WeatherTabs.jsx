import React from "react";
import WeatherCard from "./WeatherCard";
import ForecastStrip from "./ForecastStrip";

class WeatherTabs extends React.Component {

  render() {
    return (
      <div>
        <h1>Current Weather</h1>
          <section>
            <WeatherCard />
          </section>
        <h1>Forecast Weather</h1>
          <section>
            <ForecastStrip />
          </section>
      </div>
    );
  }
}

export default WeatherTabs;
