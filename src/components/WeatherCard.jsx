import React from "react";

const WeatherCard = props => {
  return (
    <div className="card-container">
      <div className="column-one">
        <h5>Icono</h5>
        <h5><strong>Temp</strong></h5>
        <h5>Descripcion</h5>
      </div>
      <div className="column-two">
        <h5><strong>Pressure:</strong>1000 hpm</h5>
        <h5><strong>Min Temp:</strong>12 °C</h5>
        <h5><strong>Sunrise:</strong>7 hs</h5>
        <h5><strong>Wind:</strong>15 Km/h</h5>
      </div>
      <div className="column-three">
        <h5><strong>Humidity:</strong>80%</h5>
        <h5><strong>Max Temp:</strong>20 °C</h5>
        <h5><strong>Sunset:</strong>21 Hs</h5>
      </div>
    </div>
  );
};

export default WeatherCard;
