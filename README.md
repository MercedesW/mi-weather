# Lab 3 - Proyecto MayWeather

## Instalación

Una sola vez:

    ```
    npm install --dev
    npm install @babel/plugin-proposal-class-properties --save-dev
    ```

y .babelrc tiene que quedar asi:

{
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react"
    ],
    "plugins": [
        [
          "@babel/plugin-proposal-class-properties"
        ]
    ]
}

    ```
    npm install babel-polyfill
    ```

Para que funcione los css importados:

    ```
    npm install css-loader style-loader --save-dev
    ```

En webpack.config.js cambiar:
    use: ['css-loader']
por:
    use: ['style-loader','css-loader']

    ```
    npm install --save moment react-moment
    ```
Para instalar Tabs(<https://github.com/reactjs/react-tabs>):

    ```
    npm install --save react-tabs
    ```

Para agregar Bootstrap:

    ```
    npm install react-bootstrap bootstrap
    ```

Para iniciar la App:

    ```
    npm start
    ```